package BD;

import java.util.List;

public interface DiskJockeyDAO {
	List<DiskJockey> findByAll();
	List<DiskJockey> findByQuery(String query);
	void insertDiskJockey(DiskJockey dj);
}
