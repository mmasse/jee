package BD;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDate;
import java.util.List;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;

@Path("/Dj-management")
public class DjController {

	private DiskJockeyDAO diskJockeyDAO = new DiskJockeyDAOImpl();
	

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/hello")
	public String hello(@Context HttpServletRequest req) {
		HttpSession session = req.getSession();
		int count = session.getAttribute("count") == null ? 0 : (int) session.getAttribute("count");

		count++;
		session.setAttribute("count", count);
		return "Hello World! -> " + count;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/diskjockey")
	public String getDiskjockey(@QueryParam("title") String title) {

		List<DiskJockey> djs = null;
		djs = diskJockeyDAO.findByAll();
		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(LocalDate.class, new LocalDateAdapter());
		Gson gson = builder.create();
		String json = gson.toJson(djs);
		return json;
	}
	
	@POST
	@Path("createdj")
	@Consumes("application/x-www-form-urlencoded")
	public void createDiskJockey(@FormParam("nom") String nom, 
	                             @FormParam("prenom") String prenom, 
	                             @FormParam("nomDeScene") String nomDeScene, 
	                             @FormParam("jour") int jour,
	                             @FormParam("mois") int mois,
	                             @FormParam("annee") int annee,
	                             @FormParam("lieuDeResidence") String lieuDeResidence, 
	                             @FormParam("styleMusical") String styleMusical) {
	    // Création de la date de naissance avec les valeurs reçues
	    LocalDate dateDeNaissance = LocalDate.of(annee, mois, jour);
	    
	    // Création d'un nouveau DiskJockey
	    DiskJockey dj = new DiskJockey(nom, prenom, nomDeScene, dateDeNaissance, lieuDeResidence, styleMusical);

	    // Insérer le DJ dans la base de données
	    DiskJockeyDAO djDAO = new DiskJockeyDAOImpl();
	    djDAO.insertDiskJockey(dj);
	    System.out.println("new book created : " + dj);
	}
	
}