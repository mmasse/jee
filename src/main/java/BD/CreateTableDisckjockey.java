package BD;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTableDisckjockey {

    public static void createTable() {
        // Obtention d'une connexion à la base de données
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();

            // Création de la requête SQL pour créer la table disckjockey
            String sql = "CREATE TABLE IF NOT EXISTS disckjockey (" +
                    "id SERIAL PRIMARY KEY," +
                    "nom VARCHAR(100) NOT NULL," +
                    "prenom VARCHAR(100) NOT NULL," +
                    "nomDeScene VARCHAR(100) NOT NULL," +
                    "dateDeNaissance DATE NOT NULL," +
                    "lieuDeResidence VARCHAR(255)," +
                    "styleMusical VARCHAR(100)" +
                    ")";

            // Création d'un objet Statement pour exécuter la requête SQL
            Statement statement = connection.createStatement();

            // Exécution de la requête SQL pour créer la table
            statement.executeUpdate(sql);

            // Fermeture de l'objet Statement
            statement.close();

            System.out.println("Table 'disckjockey' créée avec succès.");

        } catch (SQLException e) {
            System.err.println("Erreur lors de la création de la table 'disckjockey' : " + e.getMessage());
        } finally {
            // Fermeture de la connexion à la base de données
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    System.err.println("Erreur lors de la fermeture de la connexion : " + e.getMessage());
                }
            }
        }
    }

    public static void main(String[] args) {
        // Appel de la fonction pour créer la table disckjockey
        createTable();
    }
}
