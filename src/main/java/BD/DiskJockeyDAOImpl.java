package BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DiskJockeyDAOImpl implements DiskJockeyDAO {
	
	
	public List<DiskJockey> findByQuery(String query) {
        List<DiskJockey> djs = new ArrayList<>();
        
        Connection connection = DBManager.getInstance().getConnection();
        Statement statement;
		ResultSet rs;
		
        try {
        	statement = connection.createStatement();
			rs = statement.executeQuery(query);
            while (rs.next()) {
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                String nomDeScene = rs.getString("nom_de_scene");
                LocalDate dateDeNaissance = rs.getDate("date_de_naissance").toLocalDate();
                String lieuDeResidence = rs.getString("lieu_de_residence");
                String styleMusical = rs.getString("style_musical");

                DiskJockey dj = new DiskJockey(nom, prenom, nomDeScene, dateDeNaissance, lieuDeResidence, styleMusical);
                djs.add(dj);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return djs;
    }
	
	
	public List<DiskJockey> findByAll() {
		return findByQuery("select * from DJ");
	}
	
	
	// Méthode pour insérer un nouveau DJ dans la base de données
	public void insertDiskJockey(DiskJockey dj) {
	    Connection connection = null;
	    PreparedStatement statement = null;
	    
	    try {
	        connection = DBManager.getInstance().getConnection();
	        if (connection == null) {
	            throw new SQLException("La connexion à la base de données a échoué.");
	        }
	        
	        String query = "INSERT INTO DJ (nom, prenom, nom_de_scene, date_de_naissance, lieu_de_residence, style_musical) VALUES (?, ?, ?, ?, ?, ?)";
	        statement = connection.prepareStatement(query);
	        statement.setString(1, dj.getNom());
	        statement.setString(2, dj.getPrenom());
	        statement.setString(3, dj.getNomDeScene());
	        statement.setDate(4, java.sql.Date.valueOf(dj.getDateDeNaissance()));
	        statement.setString(5, dj.getLieuDeResidence());
	        statement.setString(6, dj.getStyleMusical());
	        
	        int rowsInserted = statement.executeUpdate();
	        if (rowsInserted > 0) {
	            System.out.println("Nouveau DJ ajouté à la base de données : " + dj);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	        // Ajoutez des logs ou des messages d'erreur pour mieux comprendre le problème
	    } finally {
	        // Fermez les ressources JDBC dans le bloc finally
	        try {
	            if (statement != null) {
	                statement.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        try {
	            if (connection != null) {
	                connection.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	}

}
